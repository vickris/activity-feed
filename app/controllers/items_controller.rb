class ItemsController < ApplicationController
  before_action :authenticate_user!
  
  def index
    @items = Item.order('created_at DESC')
  end

  def new
    @item = Item.new
  end

  def create
    @item = Item.new(item_params)
    if @item.save
        flash[:success] = "Item created!"
        redirect_to root_path
    end
  end

  private

  def item_params
    params.require(:item).permit(:message, :title)
  end
end